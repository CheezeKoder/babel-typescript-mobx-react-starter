module.exports = {
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif)$': 'identity-obj-proxy',
    '\\.(css)$': 'identity-obj-proxy',
    '^assets(.*)$': '<rootDir>/assets$1',
    '^src(.*)$': '<rootDir>/src$1'
  },
  rootDir: '../',
  roots: ['<rootDir>/'],
  testMatch: ['**/*.(spec|test).(ts|tsx|js)'],
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  transform: {
    '<rootDir>/(tests/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx))'
  }
};
