export const services = new Map();

export function service<T>(type: new () => T) {
  if (!services.has(type)) {
    services.set(type, new type());
  }
  return services.get(type) as T;
}
