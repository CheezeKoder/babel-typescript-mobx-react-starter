import { observable, computed, action } from 'mobx';

export class HomeStore {
  @observable private counter: number = 0;
  @observable private secretNumber = 155;

  @action
  updateNumber(num: number) {
    this.counter += num;
  }

  @action
  changeSecretNumber(num: number) {
    this.secretNumber = num;
  }

  @action
  reset() {
    this.secretNumber = 0;
    this.counter = 0;
  }

  @computed
  get Number() {
    return this.secretNumber + this.counter;
  }
}
