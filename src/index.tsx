import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { configure } from 'mobx';
import { Provider } from 'mobx-react';
import { App } from 'src/app/app';

// enable MobX strict mode

configure({
  enforceActions: 'observed'
});

// render react DOM
ReactDOM.render(<App />, document.getElementById('root'));
