import * as React from 'react';
import { service } from 'src/lib/serviceInjector';
import { HomeStore } from 'src/stores/HomeStore';
import * as style from './app.css';
import { Observer } from 'mobx-react';

export const App: React.FunctionComponent<{}> = () => {
  const homeStore = service(HomeStore);

  const [counter, setCounter] = React.useState(homeStore.Number);

  const onEnhanceButtonClick = (event: React.FormEvent<HTMLButtonElement>) => {
    homeStore.updateNumber(1);
  };

  const onResetButtonClick = (event: React.FormEvent<HTMLButtonElement>) => {
    homeStore.reset();
    setCounter(0);
  };

  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    homeStore.changeSecretNumber(parseInt(event.target.value));
    setCounter(parseInt(event.target.value));
  };

  return (
    <div className={style.home}>
      <h1>React Typescript Mobx Template with Babel/Webpack/CSS Modules</h1>
      <div>
        <div className={`${style.enhance} ${style.button}`}>
          <button type="button" onClick={onEnhanceButtonClick}>
            Enhance
          </button>
        </div>
        <div className={`${style.reset} ${style.button}`}>
          <button type="button" onClick={onResetButtonClick}>
            Reset
          </button>
        </div>
      </div>
      <Observer>
        {() => (
          <div className={`${style.observer}`}>
            <div className={`${style.total}`}>Total: {homeStore.Number}</div>
            <div>
              Set new secret number: <input type="number" onChange={onInputChange} value={counter} />
            </div>
          </div>
        )}
      </Observer>
    </div>
  );
};
