module.exports = function(api) {
  api.cache(true);

  const presets = ['@babel/preset-react', '@babel/preset-typescript'];
  const plugins = [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    '@babel/proposal-object-rest-spread',
    'react-hot-loader/babel'
  ];

  return {
    presets,
    plugins
  };
};
