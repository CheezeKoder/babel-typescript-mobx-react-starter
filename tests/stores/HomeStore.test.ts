import { HomeStore } from 'src/stores/HomeStore';

describe('Home Store', () => {
  describe('updateNumber', () => {
    let sut: HomeStore;

    beforeEach(() => {
      sut = new HomeStore();
    });

    it('should update the counter', () => {
      //Arrage
      const numberToUpdateWith: number = 5;
      //Act
      sut.updateNumber(numberToUpdateWith);
      sut.changeSecretNumber(0);
      //Assert
      expect(sut.Number).toBe(numberToUpdateWith);
    });
  });
});
